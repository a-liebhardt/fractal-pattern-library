// Custom func to build color page and configs based on yml automatically
// eslint-disable-next-line consistent-return
exports.update = function (args, done) {
  // const { fractal } = this;

  // eslint-disable-next-line global-require
  const SVGSpriter = require('svg-sprite');

  // eslint-disable-next-line global-require
  const path = require('path');
  const { sep } = path;

  // eslint-disable-next-line global-require
  const fs = require('fs');

  const spriteFilename = 'icon-sprite.svg';
  const spriteDest = 'development/icons';
  const svgSource = 'components/icons/';

  const getAllFiles = (dir) => fs.readdirSync(dir).reduce((files, file) => {
    const name = path.join(dir, file);
    const isDirectory = fs.statSync(name).isDirectory();
    return isDirectory ? [...files, ...getAllFiles(name)] : [...files, name];
  }, []);
  const files = getAllFiles(path.resolve(svgSource));

  // https://github.com/svg-sprite/svg-sprite/blob/main/docs/configuration.md
  const config = {
    shape: {
      id: {
        separator: '--',
        generator: (name) => {
          // console.log(name, file)
          const sourcePath = path.resolve(svgSource) + sep;
          const regexName = name.replace('.', '\\.');
          const regex = new RegExp(`(\\\\|\\/)${regexName}$`);
          const filePath = files.find(function (svgPath) {
            return svgPath.match(regex) ? svgPath : false;
          });
          if (filePath) {
            // eslint-disable-next-line prefer-destructuring
            name = filePath.split(sourcePath)[1];
            // eslint-disable-next-line prefer-regex-literals
            const regex2 = new RegExp('(\\\\|\\/)');
            name = path.basename(name.replace(regex2, '--').replace('.svg', ''));
          }
          return name;
        },
        whitespace: '_',
      },
    },
    svg: {
      xmlDeclaration: true,
      doctypeDeclaration: true,
      namespaceIDs: true,
      mode: {
        css: true,
        symbol: true,
      },
    },
    mode: {
      symbol: {
        dest: spriteDest,
        sprite: spriteFilename,
        example: {
          template: 'components/00-patterns/00-core/icons/_sprite-preview-template.html',
          dest: '../../components/00-patterns/00-core/icons/icons.hbs',
        },
      },
    },
  };
  const spriter = new SVGSpriter(config);

  files.forEach((file) => {
    if (file.indexOf('.svg') === -1) return;
    // eslint-disable-next-line prefer-regex-literals
    const replacer = new RegExp(/\\/, 'g');
    const relativeFilePath = file.replace(replacer, '/').split(svgSource).pop();
    const trimedRelativeFilePath = relativeFilePath.indexOf(0) === '/' ? relativeFilePath.substring(1) : relativeFilePath;
    const iconId = trimedRelativeFilePath.split('/').pop();
    const svgBody = fs.readFileSync(file, { encoding: 'utf-8' });
    spriter.add(file, iconId, svgBody);
  });

  // Compile the sprite
  spriter.compile((error, result) => {
    // Write `result` files to disk (or do whatever with them ...)
    // eslint-disable-next-line no-restricted-syntax
    for (const mode in result) {
      if (result[mode]) {
        // eslint-disable-next-line no-restricted-syntax
        for (const resource in result[mode]) {
          if (result[mode][resource] && result[mode][resource].history[0]) {
            const dest = path.resolve(result[mode][resource].history[0]);
            const dirname = path.dirname(dest);
            fs.mkdirSync(dirname, { recursive: true });
            // eslint-disable-next-line no-underscore-dangle
            fs.writeFileSync(dest, result[mode][resource]._contents);
          }
        }
      }
    }
  });

  done();
};
