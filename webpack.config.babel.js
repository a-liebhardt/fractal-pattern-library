module.exports = () => {
  // eslint-disable-next-line global-require
  const webpack = require('webpack');
  // eslint-disable-next-line global-require
  const path = require('path');
  // eslint-disable-next-line global-require
  const glob = require('glob');
  // eslint-disable-next-line global-require
  const fs = require('fs');
  // eslint-disable-next-line global-require
  const ESLintPlugin = require('eslint-webpack-plugin');
  // eslint-disable-next-line global-require
  const CopyWebpackPlugin = require('copy-webpack-plugin');
  // eslint-disable-next-line global-require
  const MiniCssExtractPlugin = require('mini-css-extract-plugin');
  // eslint-disable-next-line global-require
  const { PurgeCSSPlugin } = require('purgecss-webpack-plugin');
  // eslint-disable-next-line global-require
  const { VueLoaderPlugin } = require('vue-loader');

  const dirSrc = './components';
  const dirAssets = `${dirSrc}/assets`;
  const dirFonts = `${dirSrc}/fonts`;
  const dirIcons = `${dirSrc}/icons`;
  const dirDevelopment = './development';

  fs.mkdirSync(dirAssets, { recursive: true });
  fs.mkdirSync(dirFonts, { recursive: true });
  fs.mkdirSync(dirIcons, { recursive: true });

  let config = {};

  config = {
    entry: {
      main: './components/main.js',
      polyfills: './components/polyfills.js',
      fractal: './components/_fractal',
    },
    output: {
      filename: '[name].js',
      path: path.resolve(__dirname, dirDevelopment, 'js/'),
    },
    devtool: 'source-map',
    module: {
      rules: [
        {
          test: /\.vue$/,
          use: [
            {
              loader: 'vue-loader',
            },
          ],
          exclude: [/node_modules/],
        },
        {
          test: /\.js$/,
          use: [
            {
              loader: 'babel-loader',
            },
          ],
          exclude: [/node_modules/],
        },
        {
          test: /\.(woff2?|ttf|otf|eot)$/,
          loader: 'file-loader',
          exclude: [/node_modules/],
          options: {
            name: '[name].[ext]',
            outputPath: '../fonts/',
          },
        },
        {
          test: /\.css$/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader,
              options: {
                publicPath: '../',
              },
            },
            {
              loader: 'css-loader',
            },
          ],
        },
        {
          test: /\.scss$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].css',
                outputPath: '../css/',
              },
            },
            {
              loader: 'sass-loader',
              options: {
                sassOptions: {
                  includePaths: ['node_modules/'],
                },
              },
            },
          ],
        },
      ],
    },
    resolve: {
      extensions: ['.js'],
    },
    plugins: [
      new webpack.DefinePlugin({
        __VUE_OPTIONS_API__: true, // If you are using the options api.
        __VUE_PROD_DEVTOOLS__: false, // If you don't want people sneaking around your components in production.
      }),
      new VueLoaderPlugin(),
      new MiniCssExtractPlugin({
        filename: 'css/[name].css',
      }),
      new ESLintPlugin(),
      new PurgeCSSPlugin({
        paths: glob.sync(`${dirSrc}/**/*`, { nodir: true }),
      }),
    ],
  };

  // if directory is not empty
  if (fs.readdirSync(dirAssets).length) {
    config.plugins.push(new CopyWebpackPlugin({
      patterns: [
        {
          from: `${dirAssets}/**/*`,
          to: '../[name][ext]',
          context: __dirname,
        },
      ],
    }));
  }

  // if directory is not empty
  if (fs.readdirSync(dirFonts).length) {
    config.plugins.push(new CopyWebpackPlugin({
      patterns: [
        {
          from: `${dirFonts}/**/*`,
          to: '../fonts/[name][ext]',
          context: __dirname,
        },
      ],
    }));
  }

  // if directory is not empty
  if (fs.readdirSync(`${dirSrc}/font-awesome`).length) {
    config.plugins.push(new CopyWebpackPlugin({
      patterns: [
        {
          from: `${dirSrc}/font-awesome/webfonts/**/*`,
          to: '../webfonts/[name][ext]',
          context: __dirname,
        },
      ],
    }));
    config.plugins.push(new CopyWebpackPlugin({
      patterns: [
        {
          from: `${dirSrc}/font-awesome/css/all.min.css`,
          to: '../css/fontawesome.min.css',
          context: __dirname,
        },
      ],
    }));
  }

  if (process.env.NODE_ENV === 'production') {
    config.optimization = {
      minimize: true,
      runtimeChunk: true,
    };
  }

  return config;
};
