exports.init = (breakpoints) => {
  const getSpacer = () => {
    const div = document.createElement('div');
    div.classList.add('Pen-preview-size');
    return div;
  };

  const getButton = (breakpoint) => {
    const button = document.createElement('button');
    button.innerHTML = `${breakpoint.label}: ${breakpoint.viewport}px`;
    button.style.paddingRight = '.5rem';
    button.style.color = '#535363';
    button.style.borderRight = '.1rem solid #AAA';
    button.style.marginRight = '.5rem';
    button.addEventListener('click', (event) => {
      event.preventDefault();
      // eslint-disable-next-line no-restricted-globals
      parent.document.querySelector(
        '.Preview-wrapper.resizable',
      ).style.width = `${breakpoint.viewport}px`;
    });
    return button;
  };

  [
    // eslint-disable-next-line no-restricted-globals
    ...parent.document.querySelectorAll(
      '.Pen-panel.Pen-header .Pen-preview-size',
    ),
  ].map((element) => {
    element.classList.add('Pen-preview-size-bak');
    element.classList.remove('Pen-preview-size');
    element.before(getSpacer());
    breakpoints.forEach((breakpoint) => {
      element.before(getButton(breakpoint));
    });
    return element;
  });
};
