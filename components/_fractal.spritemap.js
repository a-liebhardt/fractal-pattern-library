exports.init = () => {
  // eslint-disable-next-line global-require
  const clipboardLocal = require('./_fractal.clipboard');

  // eslint-disable-next-line no-restricted-globals
  const parentDoc = parent.document;
  const iconList = document.querySelector('.icon-list');

  const defaultCallback = (event) => {
    event.preventDefault();
    const target = event.target.classList.contains('Tree-item') ? event.target : event.target.closest('.Tree-item');
    const treeItems = [...target.closest('.Tree-items').querySelectorAll('.Tree-item')];
    const group = target.getAttribute('data-group') === 'overview' ? null : target.getAttribute('data-group');
    treeItems.map((el) => el.classList.remove('is-current'));
    target.classList.add('is-current');
    [...iconList.querySelectorAll('.icon-list-item')].map((item) => {
      const title = item.getAttribute('title');
      if (!group) {
        item.style.display = 'inline-block';
        return item;
      }
      if (title.indexOf(group) === -1) item.style.display = 'none';
      else item.style.display = 'inline-block';
      return item;
    });
  };

  const getNavigationGroup = () => {
    const el = document.createElement('div');
    el.classList.add('Navigation-group');
    el.setAttribute('data-role', 'variant-group');
    el.setAttribute('data-component', 'icons');
    return el;
  };

  const getTree = () => {
    const el = document.createElement('div');
    el.classList.add('Tree', 'custom-icons-nav');
    return el;
  };

  const getTreeHeader = () => {
    const h3 = document.createElement('h3');
    h3.classList.add('Tree-title');
    h3.innerText = 'Icons Variants';

    const el = document.createElement('div');
    el.classList.add('Tree-header');
    el.setAttribute('data-behaviour', 'tree');
    el.append(h3);
    return el;
  };

  const getTreeItems = () => {
    const el = document.createElement('ul');
    el.classList.add('Tree-items', 'Tree-depth-1');
    return el;
  };

  const getTreeItem = (title, group, callback, active) => {
    const label = document.createElement('span');
    label.innerText = title;

    const span = document.createElement('a');
    span.classList.add('Tree-entityLink', 'icon-custom-link');
    span.setAttribute('href', '#');
    span.addEventListener('click', callback, true);
    span.append(label);

    const el = document.createElement('li');
    el.classList.add('Tree-item', 'Tree-entity');
    if (active) el.classList.add('is-current');
    el.setAttribute('data-group', group.toLowerCase());
    el.append(span);
    return el;
  };

  const setTemplate = async (groups) => {
    // eslint-disable-next-line no-restricted-globals
    const parentNavigation = parentDoc.querySelector('.Navigation-panel--variants');
    const legacyGroup = parentNavigation.querySelector('.Navigation-group[data-component="icons"]');

    const treeItems = getTreeItems();
    treeItems.append(getTreeItem('Overview', 'overview', defaultCallback, true));

    Object.keys(groups).forEach((i) => {
      const group = groups[i];
      treeItems.append(getTreeItem(`${i} (${group.length})`, `${i}`, defaultCallback));
    });

    const tree = getTree();
    tree.append(getTreeHeader());
    tree.append(treeItems);

    if (legacyGroup) {
      [...legacyGroup.querySelectorAll('.Tree')].map((el) => el.remove());
      legacyGroup.append(tree);
    } else {
      const navigationGroup = getNavigationGroup();
      navigationGroup.append(tree);
      parentNavigation.append(navigationGroup);
    }

    return true;
  };

  const getIcon = (id, size) => {
    const use = document.createElementNS('http://www.w3.org/2000/svg', 'use');
    use.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', `#${id}`);

    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    svg.append(use);

    const i = document.createElement('i');
    i.classList.add('icon', `icon--${id}`, size);
    i.append(svg);

    return i;
  };

  const printIcon = (icon) => {
    const clipboard = document.createElement('span');
    clipboard.classList.add('copy-to-clipboard');
    clipboard.setAttribute('data-clipboard', icon.id);
    clipboard.append(
      getIcon(icon.id, 'icon-xxl'),
      getIcon(icon.id, 'icon-xl'),
      getIcon(icon.id, 'icon-l'),
      getIcon(icon.id, 'icon-m'),
      getIcon(icon.id, 'icon-s'),
      getIcon(icon.id, 'icon-xs'),
      getIcon(icon.id, 'icon-xxs'),
    );

    const iconBox = document.createElement('div');
    iconBox.classList.add('icon-box');
    iconBox.append(clipboard);

    const section = document.createElement('section');
    section.append(iconBox);

    const small = document.createElement('small');
    small.innerText = icon.id;

    const el = document.createElement('div');
    el.classList.add('col-s-6', 'col-m-4', 'col-l-3', 'icon-list-item');
    el.setAttribute('title', icon.id);
    el.append(section, small);

    iconList.append(el);
  };

  const getIconGroups = async (icons) => {
    const groups = {};

    icons.forEach((icon) => {
      if (icon && icon.id) {
        printIcon(icon);
        const [group, id] = icon.id.split('--');
        if (!groups[group]) groups[group] = [];
        groups[group].push(id);
      }
    });

    return groups;
  };

  const getIconsSource = async (id) => {
    const el = document.getElementById(id);
    if (!el) throw new Error(`Element with id ${id} not found`);
    return JSON.parse(el.innerText);
  };

  const finalize = async () => {
    clipboardLocal.init();
    return true;
  };

  getIconsSource('shapes-source')
    .then(getIconGroups)
    .then(setTemplate)
    .then(finalize)
    .catch(console.warn);
};
