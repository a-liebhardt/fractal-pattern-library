// Scripts  // core version + navigation, pagination modules:
import Swiper, { Navigation, Pagination } from 'swiper';

// Core
import Layout from './00-patterns/00-core/layout/layout';

// Elements
// import Lightbox from './00-patterns/01-elements/lightbox/lightbox';

// Modules
// import Header from './00-patterns/02-modules/header/header';

// Bootstrap
import Alert from './00-patterns/05-bootstrap/alert/alert';
import Button from './00-patterns/05-bootstrap/button/button';
import Carousel from './00-patterns/05-bootstrap/carousel/carousel';
import Collapse from './00-patterns/05-bootstrap/collapse/collapse';
import Dropdown from './00-patterns/05-bootstrap/dropdown/dropdown';
import Modal from './00-patterns/05-bootstrap/modal/modal';
import Offcanvas from './00-patterns/05-bootstrap/offcanvas/offcanvas';
import Popover from './00-patterns/05-bootstrap/popover/popover';
import ScrollSpy from './00-patterns/05-bootstrap/scrollspy/scrollspy';
import Tab from './00-patterns/05-bootstrap/tab/tab';
import Toast from './00-patterns/05-bootstrap/toast/toast';
import Tooltip from './00-patterns/05-bootstrap/tooltip/tooltip';

// Modules
import Image from './00-patterns/02-modules/image/image';
import Pageheader from './00-patterns/02-modules/pageheader/pageheader';
import Pagefooter from './00-patterns/02-modules/pagefooter/pagefooter';

// JSF
import './10-jsf/02-components/vue-bootstrap-slider/bootstrap-slider';
import './10-jsf/02-components/vue-demo/demo';
import './10-jsf/02-components/vue-pageheader/pageheader';
import './10-jsf/02-components/vue-pagefooter/pagefooter';

// Custom scripts
const objectFitImages = require('object-fit-images');
// https://www.npmjs.com/package/css-element-queries
const EQ = require('css-element-queries/src/ElementQueries');
/* eslint-disable */
// const Waypoint = require('../node_modules/waypoints/lib/noframework.waypoints.js');
/* eslint-enable */

require('./main.scss');

// const ajax = new XMLHttpRequest();
// ajax.open('GET', '/icons/icon-sprite.svg', true);
// ajax.onload = function () {
//   const div = document.createElement('div');
//   div.classList.add('icon__sprite');
//   div.innerHTML = ajax.responseText;
//   document.body.insertBefore(div, document.body.childNodes[0]);
// };
// ajax.send();

function ready(fn) {
  if (document.attachEvent ? document.readyState === 'complete' : document.readyState !== 'loading') {
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }
}

[...document.querySelectorAll('.copy-to-clipboard')].map((element) => {
  element.addEventListener('click', (event) => {
    const target = event.target.classList.contains('copy-to-clipboard') ? event.target : event.target.closest('.copy-to-clipboard');
    if (target.hasAttribute('data-clipboard')) {
      const copyText = target.getAttribute('data-clipboard');
      navigator.clipboard.writeText(copyText);
    }
  });
  return element;
});

ready(() => {
  const html = document.querySelector('html');
  html.classList.remove('no-js');

  // Init polyfills
  objectFitImages();
  EQ.init();

  // Init layout helper components first
  Layout.init();
  Image.init(Swiper, Navigation, Pagination);
  Pageheader.init();
  Pagefooter.init();

  // Bootstrap
  Alert.init();
  Button.init();
  Carousel.init();
  Collapse.init();
  Dropdown.init();
  Modal.init();
  Offcanvas.init();
  Popover.init();
  ScrollSpy.init();
  Tab.init();
  Toast.init();
  Tooltip.init();
});
