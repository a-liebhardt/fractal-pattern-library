exports.init = () => {
  const getStyle = () => {
    const style = document.createElement('style');
    style.innerHTML = `
    body .dark-mode {

    }
    button.btn-darkmode {
      width: 24px;
      height: 24px;
      border-radius: 50%;
      outline: 0;
      color: black;
      background: white;
      border: 1px solid white;
      transition: color, background, border .3s ease-in-out;
      margin-left: 8px;
      box-shadow: inset 0 0 2px black;
    }
    button.btn-darkmode::after {
      content: 'L'
    }
    button.btn-darkmode:hover {
      color: white;
      background-color: black;
      border-color: white;
      box-shadow: inset 0 0 2px white;
    }
    button.btn-darkmode.active {
      color: white;
      background-color: black;
      border-color: white;
      box-shadow: inset 0 0 2px white;
    }
    button.btn-darkmode.active:hover {
      color: black;
      background-color: white;
      border-color: black;
      box-shadow: inset 0 0 2px black;
    }
    button.btn-darkmode.active::after {
      content: 'D'
    }`.replace(/\n|\s{2,}/g, '');
    return style;
  };

  const handleDarkModeBtnClick = function (event) {
    event.preventDefault();
    event.target.classList.toggle('active');
    // eslint-disable-next-line no-restricted-globals
    const iframe = parent.document.querySelector('.Preview-iframe');
    const doc = iframe.contentWindow;
    const root = doc.document.querySelector(':root');
    if (event.target.classList.contains('active')) root.classList.add('dark');
    else root.classList.remove('dark');
  };

  [
    // eslint-disable-next-line no-restricted-globals
    ...parent.document.querySelectorAll(
      '.Pen-panel.Pen-header',
    ),
  ].map((element) => {
    element.append(getStyle());
    const button = document.createElement('button');
    button.classList.add('btn-darkmode');
    button.addEventListener('click', handleDarkModeBtnClick);
    element.append(button);
    return element;
  });
};
