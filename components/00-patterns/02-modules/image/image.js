exports.init = (Swiper, Navigation, Pagination) => {
  const prepareSliderElement = async (element) => {
    // Prepare swiper setup
    const gallery = element.querySelector('.ce-outer');
    const wrapper = gallery.querySelector('.ce-inner');
    const slides = [...wrapper.querySelectorAll('.ce-row')];

    const pagination = document.createElement('DIV');
    pagination.classList.add('swiper-pagination');
    const buttonPrev = document.createElement('DIV');
    buttonPrev.classList.add('swiper-button-prev');
    const buttonNext = document.createElement('DIV');
    buttonNext.classList.add('swiper-button-next');

    gallery.classList.add('swiper');
    gallery.append(pagination, buttonPrev, buttonNext);

    wrapper.classList.add('swiper-wrapper');

    // eslint-disable-next-line no-restricted-syntax
    for (const slide of slides) {
      slide.classList.add('swiper-slide');
    }

    return gallery;
  };

  const initSlider = async (element) => {
    const slides = [...element.querySelectorAll('.swiper-slide')];

    if (slides.length > 1) {
      // Init swiper
      // eslint-disable-next-line no-unused-vars
      const swiper = new Swiper(element, {
        modules: [Navigation, Pagination],
        init: false,
        loop: true,
        slidesPerView: 1,
        spaceBetween: 0,
        pagination: {
          el: element.querySelector('.swiper-pagination'),
          type: 'bullets',
          clickable: true,
        },
        navigation: {
          nextEl: element.querySelector('.swiper-button-next'),
          prevEl: element.querySelector('.swiper-button-prev'),
        },
      });
      // swiper.on('init', function () {
      //   console.log('init');
      // });
      // init Swiper
      swiper.init();
    } else {
      element.classList.add('swiper-disabled');
      if (slides[0]) slides[0].classList.add('swiper-slide-active');
    }

    return element;
  };

  [...document.querySelectorAll('.ce-gallery')].map((element) => prepareSliderElement(element).then(initSlider).catch(console.error));
};
