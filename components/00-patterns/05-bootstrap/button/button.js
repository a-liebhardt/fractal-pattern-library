const Button = require('bootstrap/js/dist/button');

exports.init = (() => {
  [...document.querySelectorAll('.button')].map((element) => new Button(element));
});
