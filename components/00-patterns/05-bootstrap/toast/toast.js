const Toast = require('bootstrap/js/dist/toast');

exports.init = (() => {
  [...document.querySelectorAll('.toast')].map((element) => new Toast(element));
});
