const Popover = require('bootstrap/js/dist/popover');

exports.init = (() => {
  [...document.querySelectorAll('.btn-popover')].map((element) => new Popover(element));
});
