const Dropdown = require('bootstrap/js/dist/dropdown');

exports.init = (() => {
  [...document.querySelectorAll('.dropdown')].map((element) => new Dropdown(element));
});
