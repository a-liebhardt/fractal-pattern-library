const Modal = require('bootstrap/js/dist/modal');

exports.init = (() => {
  [...document.querySelectorAll('.modal')].map((element) => new Modal(element));
});
