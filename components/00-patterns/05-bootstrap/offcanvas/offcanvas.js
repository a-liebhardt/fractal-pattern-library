const Offcanvas = require('bootstrap/js/dist/offcanvas');

exports.init = (() => {
  [...document.querySelectorAll('.offcanvas')].map((element) => new Offcanvas(element));
});
