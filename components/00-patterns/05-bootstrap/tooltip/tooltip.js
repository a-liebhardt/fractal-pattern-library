const Tooltip = require('bootstrap/js/dist/tooltip');

exports.init = (() => {
  [...document.querySelectorAll('.btn-tooltip')].map((element) => new Tooltip(element));
});
