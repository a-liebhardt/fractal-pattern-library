const Collapse = require('bootstrap/js/dist/collapse');

exports.init = (() => {
  [...document.querySelectorAll('.collapse')].map((element) => new Collapse(element));
});
