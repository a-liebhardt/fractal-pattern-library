const Carousel = require('bootstrap/js/dist/carousel');

exports.init = (() => {
  [...document.querySelectorAll('.carousel')].map((element) => new Carousel(element));
});
