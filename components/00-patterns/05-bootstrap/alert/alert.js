const Alert = require('bootstrap/js/dist/alert');

exports.init = (() => {
  [...document.querySelectorAll('.alert')].map((element) => new Alert(element));
});
