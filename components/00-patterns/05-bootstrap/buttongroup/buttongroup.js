const Accordion = require('bootstrap/js/dist/accordion');

exports.init = (() => {
  [...document.querySelectorAll('.accordion')].map((element) => new Accordion(element));
});
