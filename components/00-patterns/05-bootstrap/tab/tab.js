const Tab = require('bootstrap/js/dist/tab');

exports.init = (() => {
  [...document.querySelectorAll('.tab')].map((element) => new Tab(element));
});
