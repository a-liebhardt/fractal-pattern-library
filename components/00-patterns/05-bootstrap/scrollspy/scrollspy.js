const Scrollspy = require('bootstrap/js/dist/scrollspy');

exports.init = (() => {
  [...document.querySelectorAll('.scrollspy')].map((element) => new Scrollspy(element));
});
