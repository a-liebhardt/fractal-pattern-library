exports.init = () => {
  // eslint-disable-next-line global-require
  const clipboardLocal = require('./_fractal.clipboard');

  // eslint-disable-next-line no-restricted-globals
  const parentDoc = parent.document;
  const colorList = document.querySelector('.color-list');

  const defaultCallback = (event) => {
    event.preventDefault();
    const target = event.target.classList.contains('Tree-item') ? event.target : event.target.closest('.Tree-item');
    const treeItems = [...target.closest('.Tree-items').querySelectorAll('.Tree-item')];
    const group = target.getAttribute('data-group') === 'overview' ? null : target.getAttribute('data-group');
    treeItems.map((el) => el.classList.remove('is-current'));
    target.classList.add('is-current');
    [...colorList.querySelectorAll('.color-list-item')].map((item) => {
      const title = item.getAttribute('title');
      if (!group) {
        item.style.display = 'inline-block';
        return item;
      }
      if (title.indexOf(group) === -1) item.style.display = 'none';
      else item.style.display = 'inline-block';
      return item;
    });
  };

  const getNavigationGroup = () => {
    const el = document.createElement('div');
    el.classList.add('Navigation-group');
    el.setAttribute('data-role', 'variant-group');
    el.setAttribute('data-component', 'colors');
    return el;
  };

  const getTree = () => {
    const el = document.createElement('div');
    el.classList.add('Tree', 'custom-colors-nav');
    return el;
  };

  const getTreeHeader = () => {
    const h3 = document.createElement('h3');
    h3.classList.add('Tree-title');
    h3.innerText = 'Colors Variants';

    const el = document.createElement('div');
    el.classList.add('Tree-header');
    el.setAttribute('data-behaviour', 'tree');
    el.append(h3);
    return el;
  };

  const getTreeItems = () => {
    const el = document.createElement('ul');
    el.classList.add('Tree-items', 'Tree-depth-1');
    return el;
  };

  const getTreeItem = (title, group, callback, active) => {
    const label = document.createElement('span');
    label.innerText = title;

    const span = document.createElement('a');
    span.classList.add('Tree-entityLink', 'color-custom-link');
    span.setAttribute('href', '#');
    span.addEventListener('click', callback, true);
    span.append(label);

    const el = document.createElement('li');
    el.classList.add('Tree-item', 'Tree-entity');
    if (active) el.classList.add('is-current');
    el.setAttribute('data-group', group);
    el.append(span);
    return el;
  };

  const setTemplate = async (groups) => {
    // eslint-disable-next-line no-restricted-globals
    const parentNavigation = parentDoc.querySelector('.Navigation-panel--variants');
    const legacyGroup = parentNavigation.querySelector('.Navigation-group[data-component="colors"]');

    const treeItems = getTreeItems();
    treeItems.append(getTreeItem('Overview', 'overview', defaultCallback, true));

    Object.keys(groups).forEach((i) => {
      const group = groups[i];
      treeItems.append(getTreeItem(group.prop.name, group.prop.name, defaultCallback));
    });

    const tree = getTree();
    tree.append(getTreeHeader());
    tree.append(treeItems);

    if (legacyGroup) {
      [...legacyGroup.querySelectorAll('.Tree')].map((el) => el.remove());
      legacyGroup.append(tree);
    } else {
      const navigationGroup = getNavigationGroup();
      navigationGroup.append(tree);
      parentNavigation.append(navigationGroup);
    }

    return true;
  };

  const getClipboard = (value) => {
    const el = document.createElement('span');
    el.classList.add('copy-to-clipboard');
    el.setAttribute('data-clipboard', value);
    return el;
  };

  const getColor = (color) => {
    const rgbValue = getClipboard(color.rgb);
    rgbValue.innerText = `RGB: ${color.rgb}`;

    const rgb = document.createElement('p');
    rgb.classList.add('color-rgb');
    rgb.append(rgbValue);

    const hexValue = getClipboard(color.hex);
    hexValue.innerText = `HEX: ${color.hex}`;

    const hex = document.createElement('p');
    hex.classList.add('color-hex');
    hex.append(hexValue);

    const colorIdValue = getClipboard(color.id);
    colorIdValue.innerText = `ID: ${color.id}`;

    const colorId = document.createElement('p');
    colorId.classList.add('color-id');
    colorId.append(colorIdValue);

    const name = document.createElement('small');
    name.innerText = color.name;

    const aside = document.createElement('aside');
    aside.append(
      name,
      colorId,
      hex,
      rgb,
    );

    const colorBox = document.createElement('div');
    colorBox.classList.add('color-example');
    colorBox.style.backgroundColor = color.hex;

    const el = document.createElement('div');
    el.classList.add('color-item', `level-${color.level}`);
    el.append(
      colorBox,
      aside,
    );

    return el;
  };

  const printColor = (color) => {
    const childs = document.createElement('div');
    childs.classList.add('color-box-childs');
    for (let i = 0; i < color.childs.length; i++) {
      childs.append(getColor(color.childs[i]));
    }

    const childsWrap = document.createElement('div');
    childsWrap.classList.add('col-s-12', 'col-m-6', 'col-l-8');
    childsWrap.append(childs);

    const colorBox = document.createElement('div');
    colorBox.classList.add('color-box', 'row');

    const mainColorBox = getColor(color.prop);
    mainColorBox.classList.add('col-s-12', 'col-m-6', 'col-l-4');

    colorBox.append(
      mainColorBox,
      childsWrap,
    );

    const section = document.createElement('section');
    section.append(colorBox);

    const el = document.createElement('div');
    el.classList.add('col-s-12', 'color-list-item', `level-${color.prop.level}`);
    el.setAttribute('title', color.prop.name);
    el.append(section);

    colorList.append(el);
  };

  const printColorGroups = async (colors) => {
    colors.forEach((color) => {
      printColor(color);
    });

    return colors;
  };

  const getSource = async (id) => {
    const el = document.getElementById(id);
    if (!el) throw new Error(`Element with id ${id} not found`);
    return JSON.parse(el.innerText);
  };

  const finalize = async () => {
    clipboardLocal.init();
    return true;
  };

  getSource('colors-source')
    .then(printColorGroups)
    .then(setTemplate)
    .then(finalize)
    .catch(console.warn);
};
