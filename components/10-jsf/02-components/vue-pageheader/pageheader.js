import { defineCustomElement } from 'vue';

import Pageheader from './pageheader.vue';

customElements.define('jsf-pageheader', defineCustomElement(Pageheader));
