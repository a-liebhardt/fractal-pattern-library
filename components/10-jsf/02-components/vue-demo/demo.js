import { defineCustomElement } from 'vue';

import Demo from './demo.vue';

customElements.define('jsf-demo', defineCustomElement(Demo));
