import { defineCustomElement } from 'vue';

import Pagefooter from './pagefooter.vue';

customElements.define('jsf-pagefooter', defineCustomElement(Pagefooter));
