import { defineCustomElement } from 'vue';

import BootstrapSlider from './bootstrap-slider.vue';

customElements.define('jsf-bootstrap-slider', defineCustomElement(BootstrapSlider));
