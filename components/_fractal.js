// Fractal
import Clipboard from './_fractal.clipboard';
import Breakpoints from './_fractal.breakpoints';
import Darkmode from './_fractal.darkmode';
import Spritemap from './_fractal.spritemap';
import Color from './_fractal.color';

const breakpoints = [
  {
    label: 'XS',
    viewport: 400,
  },
  {
    label: 'S',
    viewport: 600,
  },
  {
    label: 'M',
    viewport: 800,
  },
  {
    label: 'L',
    viewport: 1000,
  },
  {
    label: 'XL',
    viewport: 1200,
  },
  {
    label: 'XXL',
    viewport: 2400,
  },
];

function ready(fn) {
  if (
    document.attachEvent
      ? document.readyState === 'complete'
      : document.readyState !== 'loading'
  ) {
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }
}

ready(() => {
  Clipboard.init();
  Breakpoints.init(breakpoints);
  Darkmode.init();
  Spritemap.init();
  Color.init();
});
