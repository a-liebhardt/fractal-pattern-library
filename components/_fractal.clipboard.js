exports.init = () => {
  [...document.querySelectorAll('.copy-to-clipboard')].map((element) => {
    element.addEventListener('click', (event) => {
      const target = event.target.classList.contains('copy-to-clipboard')
        ? event.target
        : event.target.closest('.copy-to-clipboard');
      if (target.hasAttribute('data-clipboard')) {
        const copyText = target.getAttribute('data-clipboard');
        navigator.clipboard.writeText(copyText);
      }
    });
    return element;
  });
};
