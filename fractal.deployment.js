// Custom func to build color page and configs based on yml automatically
exports.update = function (args, done) {
  const { fractal } = this;

  // eslint-disable-next-line global-require
  const path = require('path');

  // eslint-disable-next-line global-require
  const fs = require('fs');

  // eslint-disable-next-line global-require
  const { Distributor } = require('distribute-files');

  const {
    UPLOAD_ENABLED,
    UPLOAD_HOST,
    UPLOAD_PORT,
    UPLOAD_USER,
    UPLOAD_PASSWORD,
    UPLOAD_REMOTE_PATH,
    UPLOAD_BATCH_SIZE,
  } = process.env;
  const UPLOAD_LOCAL_PATH = path.join(__dirname, 'dist');
  const UPLOAD_BUILD_PATH = path.join(__dirname, 'build');

  fractal.cli.log('Execute cmd build ...');
  // ChildProcess.execSync('npm run build');

  fractal.cli.log('Prepare distribution ...');
  if (fs.existsSync(`${UPLOAD_LOCAL_PATH}`)) {
    fs.rmdirSync(`${UPLOAD_LOCAL_PATH}`, { recursive: true, force: true });
  }
  fs.cpSync(`${UPLOAD_BUILD_PATH}`, `${UPLOAD_LOCAL_PATH}`, { recursive: true, force: true });
  // Remove pattern library stuff
  fs.rmdirSync(`${UPLOAD_LOCAL_PATH}${path.sep}components`, { recursive: true, force: true });
  fs.rmdirSync(`${UPLOAD_LOCAL_PATH}${path.sep}docs`, { recursive: true, force: true });
  fs.rmdirSync(`${UPLOAD_LOCAL_PATH}${path.sep}themes`, { recursive: true, force: true });
  fs.unlinkSync(`${UPLOAD_LOCAL_PATH}${path.sep}assets.html`);
  fs.unlinkSync(`${UPLOAD_LOCAL_PATH}${path.sep}docs.html`);
  fs.unlinkSync(`${UPLOAD_LOCAL_PATH}${path.sep}index.html`);
  fs.unlinkSync(`${UPLOAD_LOCAL_PATH}${path.sep}js${path.sep}fractal.js`);
  fs.unlinkSync(`${UPLOAD_LOCAL_PATH}${path.sep}js${path.sep}fractal.js.map`);

  fractal.cli.log(`Dist version ready for upload in '${UPLOAD_LOCAL_PATH}'`);

  if (!(UPLOAD_ENABLED === 'true')) done();

  if (!UPLOAD_HOST || !UPLOAD_PORT || !UPLOAD_USER || !UPLOAD_PASSWORD) {
    fractal.cli.error('Invalid SFTP Configuration. Please check your ENV!');
    fractal.cli.error('Abort Deployment Operation');
    done();
  }

  fractal.cli.log('Prepare upload');
  fractal.cli.log(`Source '${UPLOAD_LOCAL_PATH}'`);
  fractal.cli.log(`Destination '${UPLOAD_REMOTE_PATH}'`);

  /**
   * Find all files inside a dir, recursively.
   * @function getAllFiles
   * @param  {string} dir Dir path string.
   * @return {string[]} Array with all file names that are inside the directory.
   */
  const getAllFiles = (dir) => {
    return fs.readdirSync(dir).reduce((files, file) => {
      const fileName = path.join(dir, file);
      const isDirectory = fs.statSync(fileName).isDirectory();
      return isDirectory ? [...files, ...getAllFiles(fileName)] : [...files, fileName];
    }, []);
  };

  const sftpPort = 22;
  let files = getAllFiles(UPLOAD_LOCAL_PATH);
  files = files.map((file) => file.split(`dist${path.sep}`)[1]);
  const fileBatches = [];
  for (let i = 0; i < files.length; i += UPLOAD_BATCH_SIZE) {
    fileBatches.push(files.slice(i, i + UPLOAD_BATCH_SIZE));
  }
  fractal.cli.log(`Files Total '${files.length}'`);
  fractal.cli.log(`Batches Total '${fileBatches.length}'`);

  // Handle upload
  const config = {
    debug: true,
    root: `${UPLOAD_LOCAL_PATH}${path.sep}`,
    servers: [],
  };
  if (UPLOAD_PORT === sftpPort) {
    config.servers.push({
      type: 'sftp',
      connection: {
        host: UPLOAD_HOST,
        port: 22,
        user: UPLOAD_USER,
        password: UPLOAD_PASSWORD,
      },
    });
  } else {
    config.servers.push({
      type: 'ftp',
      connection: {
        host: UPLOAD_HOST,
        port: 21,
        user: UPLOAD_USER,
        password: UPLOAD_PASSWORD,
      },
      root: UPLOAD_REMOTE_PATH,
    });
  }

  const distributor = new Distributor(config);

  const uploadFiles = function (i) {
    if (i + 1 >= fileBatches.length) done();
    fractal.cli.log(`Upload batch ${i + 1}`);
    distributor
      .distributeFiles(fileBatches[i])
      .then((success) => {
        if (success) fractal.cli.log(success);
        uploadFiles(i + 1);
      }, (error) => {
        fractal.cli.error(error);
        uploadFiles(i + 1);
      })
      .catch((error) => {
        fractal.cli.error(error);
        uploadFiles(i + 1);
      });
  };
  uploadFiles(0);
};
