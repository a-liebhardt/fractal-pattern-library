// Custom func to publish to gh-pages
exports.update = function (args, done) {
  const { fractal } = this;

  // eslint-disable-next-line global-require
  const ghpages = require('gh-pages');

  const dist = fractal.web.get('builder.dest');
  const config = {
    // branch: 'main',
    // repo: 'https://example.com/other/repo.git'
  };
  const callback = (err) => {
    if (err) fractal.cli.error(err);
    fractal.cli.log('Update to gh-pages done.');
  };

  ghpages.publish(dist, config, callback);

  done();
};
