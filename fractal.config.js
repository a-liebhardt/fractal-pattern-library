/*
 * Require the path module
 */
const path = require('path');

require('dotenv').config();

/*
 * Require the Fractal module
 */
// eslint-disable-next-line no-multi-assign
const fractal = (module.exports = require('@frctl/fractal').create());

const mandelbrot = require('@frctl/mandelbrot');

/*
 * Require package configuration
 */
// eslint-disable-next-line import/no-dynamic-require
const packagejson = require(path.join(__dirname, 'package.json'));

/*
 * Give your project configuration
 */
fractal.set('project.title', packagejson.title);
fractal.set('project.version', packagejson.version);

/*
 * Tell Fractal where to look for components.
 */
fractal.components.set('path', path.join(__dirname, 'components'));
fractal.components.set('exclude', ['**/node_modules/**']);
fractal.components.set('label', 'Project');
fractal.components.set('default.status', packagejson.fractal.statuses.default);
fractal.components.set('statuses', packagejson.fractal.statuses.options);
/*
 * Tell Fractal where to look for documentation pages.
 */
fractal.docs.set('path', path.join(__dirname, 'docs'));

/*
 * Tell the Fractal web preview plugin where to look for static assets.
 */
let devBrowsers = ['chrome', 'firefox', 'safari'];
if (process.env.FRACTAL_BROWSERS && process.env.FRACTAL_BROWSERS.length) {
  devBrowsers = process.env.FRACTAL_BROWSERS.split(',');
}

fractal.web.set('static.path', path.join(__dirname, 'development'));
fractal.web.set('builder.dest', path.join(__dirname, 'build'));
fractal.web.set('server.sync', true);
fractal.web.set('server.syncOptions', {
  watchOptions: {
    ignoreInitial: true,
    ignored: ['**/*.scss', '**/*.vue'], // ignore the files you want webpack HMR to take care of
  },
  open: true,
  browser: devBrowsers,
  notify: true,
});

// Extend highlight.js handling to prevent cli warning 'Could not find the language 'vue', ...'
const hljs = require('highlight.js');
const hljsSvelte = require('highlightjs-svelte');
const _ = require('lodash');

hljsSvelte(hljs);
fractal.web.set('highlighter', function (content, options) {
  const language = options.language
    ? options.language.toLowerCase()
    : options.language;
  content = _.toString(content || '');
  if (language === 'vue') return hljs.highlightAuto(content).value;
  try {
    return language
      ? hljs.highlight(language, content).value
      : hljs.highlightAuto(content).value;
  } catch (e) {
    return hljs.highlightAuto(content).value;
  }
});

/*
 * Theme
 */
const theme = {
  skin: {
    name: 'white',
    accent: '#FFF',
    complement: '#444',
    links: '#444',
  },
  navigation: 'split',
  rtl: false,
  lang: 'en',
  styles: 'default',
  highlightStyles: 'default',
  scripts: 'default',
  format: 'json',
  static: {
    mount: 'themes/mandelbrot',
  },
  version: packagejson.version,
  favicon: null,
  information: [
    {
      label: 'Version',
      value: packagejson.version,
    },
    {
      label: 'Built on',
      value: new Date(),
      type: 'time',
      format: (value) => {
        return value.toLocaleDateString();
      },
    },
  ],
  labels: {
    info: 'Information',
    builtOn: 'Built on',
    search: {
      label: 'Search',
      placeholder: 'Search…',
      clear: 'Clear search',
    },
    navigation: {
      back: 'Back',
    },
    tree: {
      collapse: 'Collapse tree',
    },
    components: {
      handle: 'Handle',
      tags: 'Tags',
      variants: 'Variants',
      context: {
        empty: 'No context defined.',
      },
      notes: {
        empty: 'No notes defined.',
      },
      preview: {
        label: 'Preview',
        withLayout: 'With layout',
        componentOnly: 'Component only',
      },
      path: 'Filesystem Path',
      references: 'References',
      referenced: 'Referenced by',
      resources: {
        file: 'File',
        content: 'Content',
        previewUnavailable:
          'Previews are currently not available for this file type.',
        url: 'URL',
        path: 'Filesystem Path',
        size: 'Size',
      },
    },
    panels: {
      html: 'HTML',
      view: 'View',
      context: 'Context',
      resources: 'Resources',
      info: 'Info',
      notes: 'Notes',
    },
  },
};
fractal.web.theme(mandelbrot(theme));

// Extend fractal with custom functions

const color = require('./fractal.color');

fractal.cli.command('fractal:color', color.update);

const svg = require('./fractal.svg');

fractal.cli.command('fractal:svg', svg.update);

const deploy = require('./fractal.deployment');

fractal.cli.command('fractal:deploy', deploy.update);

const ghPages = require('./fractal.gh-pages');

fractal.cli.command('fractal:gh-pages', ghPages.update);

// Events
// fractal.load().then(() => {
//   fractal.cli.log('Finished parsing components and documentation!');
// });

// fractal.on('source:loaded', function (source) {
//   fractal.cli.log(`${source.name} has been loaded`);
// });

// // eslint-disable-next-line no-unused-vars
// fractal.on('source:changed', function (source, eventData) {
//   fractal.cli.log(`Change in ${source.name} directory`);
// });

// // eslint-disable-next-line no-unused-vars
// fractal.on('source:updated', function (source, eventData) {
//   fractal.cli.log(`${source.name} has been updated`);
// });
