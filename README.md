# Fractal Pattern Library

The pattern library connects Customer, PM, Dev and Layout Departments. It there for lives in between the layout and implementation process.

As part of that it collects, lists and documents all frontend elements, modules and templates. The useage of Mockups helps to showcase and simulate the expected behaviour. 
It unifies the implementation process, works as communication baseline, provides a smoother way for implementation and ease development while reduce technical overhead.

> This Pattern Library is build upon [Fractal Pattern Library](https://fractal.build/). Check out [Fractal GitHub](https://github.com/frctl/fractal) for more informations.

---

Index

1. [Project Structure](#project-structure)
2. [How to start](#how-to-start)
3. [How to use](#how-to-use)
4. [How to develop](#how-to-develop)
5. [Configuration Files](#configuration-files)
6. [CLI](#cli)
7. [External Resources](#external-resources)
8. [Contributors](#contributors)
9. [License](#license)

---

<a name="project-structure"></a>

## Project Structure

```
├─ build                    // library build ready for depolyment
├─ components
│   ├─ /_config             // general library setup
│   ├─ /00-patterns
│   │   ├─ /00-core         // general configurations
│   │   ├─ /01-elements     // elements (context independend and generic)
│   │   │   └─ ...
│   │   ├─ /02-modules      // modules (context independend, but specific)
│   │   │   └─ ...
│   │   └─ /03-templates    // templates (context bound and very specific)
│   │       └─ ...
│   ├─ /10-jsf              // javascript frame work
│   │   └─ ...
│   ├─ /assets              // static project assets
│   ├─ /fonts               // project fonts
│   ├─ /icons               // project svgs
│   ├─ /images              // project images
│   ├─ _fractal.js          // library logic
│   ├─ _preview.hbs         // base render page
│   ├─ main.js              // project logic
│   ├─ main.scss            // project style
│   └─ polyfills.scss       // browser fixes
├─ dist                     // project build ready for depolyment (stripped by all library stuff)
├─ docs                     // library documentation
│   └─ 01-index.md          // the very first page you'll see on open the pattern library
│   └─ categories
│       └─ example.md
├─ development              // development storage
├─ fractal.color.js         // custom library extension: color management
├─ fractal.config.js        // basic library configuration
├─ fractal.deployment.js    // custom library extension: deployment management
└─ webpack.config.babel.js  // basic webpack configuration
```

<a name="how-to-start"></a>

## How to start

Clone the repo and navigate to your destination folder. Then install the project via `npm i`. The latest Node Version is required.

<a name="how-to-use"></a>

## How to use

To start development run `npm start`. This will process your configuration and open a new Tab in Chrome, Firefox and Safari for development.

> Notice: You can configure the requested browsers at line 45 in `fractal.config.js`

To create a new build rund `npm run build`. This command will collect and prepare all files for deplyoment. The files will be stored it in the `dist/` folder.

<a name="how-to-develop"></a>

## How to develop

All patterns are stored in the components directory and organized in Core, Elements, Modules and Templates

### Config

General configurations to manage and organize the library. You can find a lot of utilities and other tools here.

- functions.scss
- mixins.scss
- settings.scss
- utilities.scss

### Patterns

#### Core

All the generic setup required for the project.

> Like colors, fonts, icons, ...
#### Elements

A Element is context independent and represent the most generic, flexible and reusable asset in the project.

> Like a generic button or form asset

#### Modules

A Module represent a more complex asset and can be build with various Elements and other Modules. It is specific so it can't be used as freely as an Element, but still works context independend, is flexible and reusable.

> Like a complete form, build with various form assets and button

#### Templates

A Template is the most restricted asset. It is very specific and not very flexible nor reusable. It is build with various Elements, Modules and other Templates.

> Like a whole register page, build with Module and Element assets

#### Bootstrap

A example integration of a 3th party setup to this library. All styles and scripts are organized in the respective element configuration and are registered separately. This example shows the usage of a 3th party in the least invasiv way.

### Javascript Framework

The library showcase teh integration of [VUE](https://vuejs.org/) as a Javascript Framwork. Integration is made with [Custom Elements](https://vuejs.org/guide/extras/web-components.html#using-custom-elements-in-vue)

#### Custom Element

Example shows how a Custom Element can be registered and style can be passed down.

```
import { defineCustomElement } from 'vue'; // Import vue setup

import Demo from './demo.vue'; // Import element

const styles = [];
styles.push('@import url(/css/main.css)'); // Import project style 

customElements.define('jsf-demo', defineCustomElement({ ...Demo, styles })); // Register Element as Custom Element
```

### Assets

All static files which will be placed at the root of the project. 

> Like the favicon

### Fonts

All font files placed in `/fonts` will be made available to the project. Checkout `/components/detail/fonts` to see all available fonts.

> To register a new font add it to `_fonts.scss` and extend `fonts.config.yml` to recieve a font preview.

> If you add new fonts you might have to restart the Pattern Library to update the project.

### Icons

All icons placed in `/icons` will be add to the icon sprite file. The icons can be viewed at `/components/detail/icons--default`.

> If you add new icons you might have to restart the Pattern Library to update the project.

### Images

All static images placed at `/images` will be made available to the project via `/images/<image-name>.<image-suffix>`.

<a name="configuration-files"></a>

## Configuration files

### main.scss

The main style registration file for the project. Only styles applied here will be processed.

### main.js

The main script registraion file. Only scripts applied here will be processed.

### polyfills.js

All polyfills required for the project you can find here.

<a name="cli"></a>

## CLI

### Before start

Copy and rename `.env-dist` to `.env`. Make your configurations here as desired.

```
FRACTAL_BROWSERS=chrome,firefox,safari  // define the browser to open in development
UPLOAD_ENABLED=true                     // enable upload to UPLOAD_HOST in cmd deploy
UPLOAD_HOST=                            // FTP/SFTP host
UPLOAD_PORT=                            // 21: FTP / 22: SFTP
UPLOAD_USER=                            // Username
UPLOAD_PASSWORD=                        // Password
UPLOAD_REMOTE_PATH="/"                  // The remote path to upload into
UPLOAD_BATCH_SIZE=10                    // The upload batch size
```

### Install the project

```
npm i 
```

### Start the project

```
npm start
```

### Build new Static Version

```
npm run build 
```

### Upload Static Version to GH-Pages

```
npm run gh-pages 
```

> Integration is not final

### Build+Upload new Deployment Version

```
npm run deploy
```

Set `UPLOAD_ENABLED` to `true` to enable auto upload. Set it to `false` to only prepare the dist configuration files. You then can upload the `/dist` assets manually.

<a name="external-resources"></a>

## External Resources

Repositories and Sources

- [Fractal Pattern Library](https://fractal.build/)
- [Font Awesome](https://fontawesome.com/download)

Online Generators

- [Favicon Generator](https://www.favicon-generator.org/)
- [Modern and simple css @font-face generator](https://transfonter.org/)
- [Color Gradient Generator](https://colordesigner.io/gradient-generator)

<a name="contributors"></a>

## Contributors 

Thanks to the fractal team for that awesome ground work.

<a name="license"></a>

## License

MIT