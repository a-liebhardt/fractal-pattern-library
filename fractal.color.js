// Custom func to build color page and configs based on yml automatically
// eslint-disable-next-line consistent-return
exports.update = function (args, done) {
  // const { fractal } = this;

  // eslint-disable-next-line global-require
  const os = require('os');

  // eslint-disable-next-line global-require
  const yaml = require('yamljs');

  // eslint-disable-next-line global-require
  const fs = require('fs');

  // Array that stores color variables so they can be referenced by other variables
  const endOfLine = os.EOL;
  const colorsStartDelimiter = `// COLORS (START) ${endOfLine}// Auto-Generated section, please edit in src/atoms/colors/colors.yml ${endOfLine}// Use SASS function rgba, darken, lighten ... to adjust color to your needs. ${endOfLine}// For more Info see: https://robots.thoughtbot.com/controlling-color-with-sass-color-functions`;
  const colorsEndDelimiter = '// COLORS (END)';
  const colorsArray = [];
  const fileYML = './components/00-patterns/00-core/colors/colors.config.yml';
  const sourceHBS = './components/00-patterns/00-core/colors/_colors-preview-template.html';
  const targetHBS = './components/00-patterns/00-core/colors/colors.hbs';
  const fileSettings = './components/_config/_settings.scss';

  let currentColor;
  let currentColorId;
  let rgbValue;
  let colorSection;
  let segments;
  let segmentColor;
  let segmentName;
  let newColorsSection = colorsStartDelimiter + endOfLine;

  const colorsYAML = yaml.load(fileYML);

  // const getColorPartial = (
  //   colorPartName,
  //   colorPartId,
  //   colorPartHEX,
  //   colorPartRGB,
  //   level,
  // ) => {
  //   const colorId = colorPartId.toLowerCase().replace('$', '');
  //   let colorPart = '';
  //   colorPart += `<div class='color-item level-${level}'>${endOfLine}`;
  //   colorPart += `<section class='color-example' style='background-color: ${colorPartHEX};'></section>${endOfLine}`;
  //   colorPart += `<aside>${endOfLine}`;
  //   colorPart += `<p>${colorPartName}</p>${endOfLine}`;
  //   colorPart += `<p class='color-id'><span class='copy-to-clipboard' data-clipboard='$${colorId}'>ID: ${colorId}</span></p>${endOfLine}`;
  //   colorPart += `<p class='color-hex'><span class='copy-to-clipboard' data-clipboard='${colorPartHEX}'>HEX: ${colorPartHEX}</span></p>${endOfLine}`;
  //   colorPart += `<p class='color-rgb'><span class='copy-to-clipboard' data-clipboard='${colorPartRGB.replace(/\//g, ', ')}'>RGB: ${colorPartRGB}</span></p>${endOfLine}`;
  //   colorPart += `</aside>${endOfLine}`;
  //   colorPart += `</div>${endOfLine}`;
  //   return colorPart;
  // };

  const hexRgb = function (hex) {
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result
      ? { r: parseInt(result[1], 16), g: parseInt(result[2], 16), b: parseInt(result[3], 16) }
      : null;
  };

  const rgbToHex = function (r, g, b) {
    function componentToHex(c) {
      const hex = c.toString(16);
      return hex.length === 1 ? `0${hex}` : hex;
    }
    return `#${componentToHex(r)}${componentToHex(g)}${componentToHex(b)}`;
  };

  const colors = [];
  const loopObject = (items, level, parentId, colorId) => {
    Object.keys(items).forEach((section) => {
      let currentId = parentId;

      if (level === 0) {
        // Draw group header
        newColorsSection += `//--- ${section.toUpperCase()} ---//${endOfLine}`;

        if (section.endsWith('-')) {
          // do not created next currentId
          currentId = colors.length - 1;

        } else {
          colors.push({
            prop: {},
            childs: [],
          });
          currentId = colors.length - 1;
        }
      }
      // Read color object
      colorSection = items[section];

      // Set color ID
      currentColorId = colorId + section.toLowerCase();

      // Parse each colors in the section
      if (typeof colorSection === 'object') {
        loopObject(colorSection, level + 1, currentId, currentColorId);

      } else {
        segments = colorSection.split(';');
        segmentColor = segments[0] ? segments[0] : colorSection;
        segmentName = segments[1] ? segments[1] : section;

        // Check if another color variable is referenced to
        // (must be in the same file and must be defined first)
        currentColor = segmentColor.startsWith('$')
          ? colorsArray[segmentColor]
          : segmentColor;

        // Store color in colors array (in case referenced later as a variable by another color)
        colorsArray[currentColorId] = currentColor;

        // Get RGB
        if (currentColor.startsWith('rgb')) {
          const rgb = currentColor.replace(/[rgba() ]/gi, '').split(','); // replace(/[rgba\(\) ]/gi, '').split(',');
          rgbValue = [parseInt(rgb[0]), parseInt(rgb[1]), parseInt(rgb[2])];
          // Convert HEX to RGB
        } else {
          rgbValue = hexRgb(currentColor);
        }

        // Add to color template
        const hexLabel = rgbToHex(rgbValue[0], rgbValue[1], rgbValue[2]);
        const rgbLabel = rgbValue.join('/');
        if (level === 0) {
          colors[currentId].prop = {
            name: segmentName,
            id: currentColorId,
            hex: hexLabel,
            rgb: rgbLabel,
            level,
          };
        } else {
          colors[currentId].childs.push({
            // name: `${parentId}${segmentName}`,
            name: `${segmentName}`,
            id: currentColorId,
            hex: hexLabel,
            rgb: rgbLabel,
            level,
          });
        }

        // Create the RGB variable
        newColorsSection += `${currentColorId}: rgb(${rgbValue.join(', ')}) !default;${endOfLine}`;
      }
    });
  };

  loopObject(colorsYAML.context.colors, 0, null, '$color--');

  newColorsSection += colorsEndDelimiter;

  // Update color variables in settings
  let settingsContent = fs.readFileSync(fileSettings, 'utf8');
  settingsContent = settingsContent.replace(
    /\/\/ COLORS \(START\)(.|\n|\r\n)*\/\/ COLORS \(END\)/,
    newColorsSection,
  );
  fs.writeFileSync(fileSettings, settingsContent);
 
  // Update color preview HTML
  let colorsBody = fs.readFileSync(sourceHBS, { encoding: 'utf-8' });
  colorsBody = colorsBody.replace('{{colors}}', JSON.stringify(colors));
  fs.writeFileSync(targetHBS, colorsBody);

  done();
};
